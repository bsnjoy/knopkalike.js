var config = require('./config.json');
var fs = require('fs');
var request = require("request");

var flashing = false, flashbutton;
var button_led_pin = [13, 12]; // Пины светодиодов на кнопках 0 и 1
 
var INPUT = 0;
var OUTPUT = 1;
var LOW = 0;
var HIGH = 1;

function getDateTime() {

    var date = new Date();

    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;

    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    var year = date.getFullYear();

    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    return year + ":" + month + ":" + day + " " + hour + ":" + min + ":" + sec;

}

function log( txt ) {
    var time = getDateTime();
    console.log(time + ' ' + txt);
}

function analogRead(pin)
{
    var output = fs.readFileSync("/proc/adc"+pin, "utf8");
    var values = output.split(":");
    var result = Number(values[values.length - 1]);

    //console.log(result);
    return result
}

function digitalWrite(pin, value)
{
    fs.writeFileSync("/sys/devices/virtual/misc/gpio/pin/gpio" + pin, String(value));
}
 
function pinMode(pin, mode)
{
    fs.writeFileSync("/sys/devices/virtual/misc/gpio/mode/gpio" + pin, String(mode));
}
 
function delay(ms) 
{
    var start = new Date().getTime();
    while(1)
    {         
	    if ((new Date().getTime() - start) > ms)
        {
              break;
        }
    }
}
 
function setup()
{
    pinMode(button_led_pin[0], OUTPUT);
    pinMode(button_led_pin[1], OUTPUT);
}
 
var buttonNotPressed = [ true, true ];

function checkButton(button)
{
    if( analogRead(2+button)>4000 )
    {
        if( buttonNotPressed[button] )
        {
            log("Button" + button + "pressed!");
            request("http://knopkalike.ru/add.php?device=" + config.shop + "&value="+(1+button), function(error, response, body) {
//                console.log("Details:");
//                console.log(body);
//                console.log(response);
//                console.log(error);
            });
            flashing = true;
            flashbutton = button;
            digitalWrite(button_led_pin[1-button], LOW);
            setTimeout( function(){flashFunction(20);}, 0);
        }
        buttonNotPressed[button] = false;
    } else {
        buttonNotPressed[button] = true;
    }
}

var flashlast = HIGH; // последнее состояние вспышки
function flashFunction(times)
{
    //console.log(times);
    digitalWrite(button_led_pin[flashbutton], flashlast);
    flashlast = 1 - flashlast;
    if( times > 0 )
    {
        setTimeout( function(){flashFunction(times-1);}, 100);
        //setTimeout(flashFunction(times-1), 1000);
    } else {
        flashing = false;
    }
}
    //for(i=0;i<10;i++)
            //{
             //   delay(100);
            //    digitalWrite(button_led_pin[button], LOW);
               // delay(100);
            //}

function loop()
{
    
    if(!flashing) // Быстрое мигание нажатой кнопки. Пока это происходит второй раз кнопку не нажимаем
    {
        if(Math.round(new Date().getTime()/1000)%2)
        {
            digitalWrite(button_led_pin[0], HIGH);
            digitalWrite(button_led_pin[1], HIGH);
        } else {
            digitalWrite(button_led_pin[0], LOW);
            digitalWrite(button_led_pin[1], LOW);
        }
        checkButton(0);
        checkButton(1);
    }
//    console.log('HIGH');
//    delay(1000);
//    digitalWrite(led_pin, LOW);
//    console.log('LOW');
//    delay(1000);
}
 
function main()
{
    log(buttonNotPressed);
    log(button_led_pin[0]);
    setup();
    setInterval( loop, 0);
}

main();
