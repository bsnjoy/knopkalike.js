# Установки:
sudo apt-get install -y vim git wget
# vim - для редактирования файлов на самаой кнопке в будующем
# git - чтобы скачать ПО из репозитория
# wget - для скачивания node.js с сервера

# Install node.js on ARM compilled version is the easiest way
wget http://nodejs.org/dist/v0.10.26/node-v0.10.26-linux-arm-pi.tar.gz
tar xvzf node-v0.10.26-linux-arm-pi.tar.gz
cd node-*
# Remove some extra files that we don't need
rm ChangeLog LICENSE README.md
# Install in system (just copy)
sudo cp -R * /usr/local

# Install knopka programm
# ssh -o BatchMode=yes -o StrictHostKeyChecking=no git@bitbucket.org "uptime"
# git clone git@bitbucket.org:bsnjoy/knopkalike.js.git
cd ..
wget https://bitbucket.org/bsnjoy/knopkalike.js/get/73c5389389ae.zip
unzip 73c5389389ae.zip
mv bsnjoy-knopkalike.js-73c5389389ae code

# Add to autorun
sudo sh -c 'echo "@/home/ubuntu/code/startknopka" >> /etc/xdg/lxsession/Lubuntu/autostart'
